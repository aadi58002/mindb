* Why ?
The first question arises. why do this in the first place aren't there already more than enough database which are well built and community supported ?  
My answer is yes there are many good even great infact database out there but i am just doing this to learn about database and rust.

* Planned Features
- CREATE DATABASE {database name}
- USE {database name}
- LISTDB
- CREATE TABLE {table name} {
    col1 type1,
    col2 type2,
    col3 type3,
  }
  - types -> varchar(Max fixed length),int
- SELECT {\*|{col name}} FROM {table name}
- INSERT INTO {table name} ({col1},{col2}...) 
- DELETE FROM {table name} ({col1},{col2}...) 
- DROP TABLE {table name}
- DROP DATABASE {database name}
